﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterCompanyViewModel.Admin
{
  public  class OrderFM
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Note { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
