﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterCompanyViewModel.Admin
{
   public class DriverVM
    {
        public int Id { get; set; }
        public string NameAr { get; set; }
        public string Telephone { get; set; }
        public string Address { get; set; }
        public decimal PercentAmount { get; set; }
        public string Area { get; set; }
        public string Manager { get; set; }
        public string Car { get; set; }
        public string Note { get; set; }
        public string RegionName { get; set; }
    }
}
