﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterCompanyViewModel.Admin
{
   public class CustomerVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Longid { get; set; }
        public string Latid { get; set; }
        public string Area { get; set; }
        public string PhoneNumber { get; set; }
        public string RegionName { get; set; }
    }
}
