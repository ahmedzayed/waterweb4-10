﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterCompanyViewModel.Admin
{
   public class InvoiceFM
    {
        public int Id { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string Note { get; set; }
        public int DelegateId { get; set; }
        public int DriverId { get; set; }
        public int PaymentId { get; set; }
        public int TypeId { get; set; }
        public int ProductId { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public int InvoiceId { get; set; }
    }
}
