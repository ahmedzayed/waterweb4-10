﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterCompanyViewModel.Admin
{
   public class AgreementFM
    {
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Area { get; set; }
        public int DelegatId { get; set; }
        public int CarId { get; set; }
        public int DriverId { get; set; }
        public string Note { get; set; }
        public int ProductId { get; set; }
        public decimal Price { get; set; }
        public string Target { get; set; }
        public int RequestId { get; set; }
        public int RegionId { get; set; }

    }
}
