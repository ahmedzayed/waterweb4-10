﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterCompanyViewModel.Admin
{
   public class ProductVM
    {
        public int Id { get; set; }
        public string NameAr { get; set; }
        public string DescAr { get; set; }
        public decimal Price { get; set; }
        public string Note { get; set; }

        public string Image { get; set; }
        public decimal Disscount { get; set; }
        public DateTime DisscountEndDate { get; set; }

    }
}
