﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterCompanyViewModel.Admin
{
  public  class AboutUsVM
    {
        public int Id { get; set; }
        public string Address { get; set; }
        public string Phonenumber { get; set; }
        public string AboutDetails { get; set; }

    }
}
