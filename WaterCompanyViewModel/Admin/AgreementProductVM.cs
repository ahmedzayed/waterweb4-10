﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterCompanyViewModel.Admin
{
  public  class AgreementProductVM
    {
        public int Id { get; set; }
        public decimal   Price { get; set; }
        public string Target { get; set; }
        public string Name { get; set; }
    }
}
