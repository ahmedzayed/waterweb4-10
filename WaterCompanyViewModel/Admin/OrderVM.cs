﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterCompanyViewModel.Admin
{
   public class OrderVM
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public string Note { get; set; }
        public DateTime OrderDate { get; set; }
        public decimal Disscount { get; set; }
        public int RegionId { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
