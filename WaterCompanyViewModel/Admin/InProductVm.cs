﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterCompanyViewModel.Admin
{
  public  class InProductVm
    {
        public int Id { get; set; }
        public string NameAr { get; set; }
        //public string Name { get; set; }
        public string DescAr { get; set; }
        public string DescEn { get; set; }
        //public string Description { get; set; }
        public decimal Price { get; set; }
        public string Note { get; set; }

        //public string Target { get; set; }

        public decimal Quantity { get; set; }
        public decimal TotalPrice { get; set; }
        public string DelegatePercent { get; set; }
        public string DriverDelegate { get; set; }



        public decimal ManagerPercent { get; set; }
        public decimal ManagerAssistantPercent { get; set; }
        public decimal DelegetPercent { get; set; }
        public decimal DriverPercent { get; set; }
    }
}
