﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaterCompanyViewModel.Admin
{
   public class CarsVM
    {
        public int Id { get; set; }
        public string TypeCars { get; set; }
        public string ColorCars { get; set; }
        public string ModelCars { get; set; }
        public string NumberCars { get; set; }
        public string NoteCars { get; set; }

    }
}
