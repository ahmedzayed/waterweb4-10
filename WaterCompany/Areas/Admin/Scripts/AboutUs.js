﻿var AboutUs = {
    init: function () {
        Search();
    },
}

function Search() {

    var $form = $("#SearchForm");

    var data = new FormData();
    var formData = $form.serializeArray();
    $.each(formData, function (key, value) {
        data.append(this.name, this.value);
    });

    ajaxRequest('Post', $("#SearchForm").attr('action'), data, 'html', false, false).done(function (result) {
        $("#SearchTableContainer").html(result);
    });
}
//function SearchAll() {
//    var $form = $("#SearchForm");

//    var data = new FormData();

//    $('#NameSm').val("");


//    var formData = $form.serializeArray();

//    ajaxRequest('Post', $("#SearchForm").attr('action'), data, 'html', false, false).done(function (result) {
//        $("#SearchTableContainer").html(result);
//    });
//}


function Save() {

    var $form = $("#ModelForm");
    //if ($form.valid()) {
    var data = new FormData();
    var formData = $form.serializeArray();
    $.each(formData, function (key, value) {
        data.append(this.name, this.value);
    });

    if (IsValid()) {
        ajaxRequest($("#ModelForm").attr('method'), $("#ModelForm").attr('action'), data, 'json', false, false).done(function (result) {

            var res = result.split(',');
            //debugger;
            if (res[0] == "success") {
                $('#myModalAddEdit').modal('hide');
                toastr.success(res[1]);
                Search();
                Clear();


            }
            else
                toastr.error(res[1]);
        });
    }
    //}
}

function Create() {
    var Url = "../AboutUs/Create";
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function Edit(id) {
    var Url = "../AboutUs/Edit?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function Delete(id) {
    var Url = "../AboutUs/Delete?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function DeleteRow(id) {
    var Url = "../AboutUs/DeleteRow?Id=" + id;
    ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
        var res = result.split(',');
        if (res[0] == "success") {
            $('#myModalAddEdit').modal('hide');
            Search();
        }
        else
            toastr.error(res[1]);
    });
}
function IsValid() {
    var isValidItem = true;

    //if ($('#Type').val() == "") {
    //    isValidItem = false;
    //    toastr.error("من فضلك ادخل  النوع");
    //}



    return isValidItem;
}
function Clear() {
    $('#Type').val() = "";

}




