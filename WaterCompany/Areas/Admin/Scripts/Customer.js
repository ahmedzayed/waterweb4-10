﻿var manageCustomers = {
    init: function () {
        Search();
    },
}

function Search() {

    var $form = $("#SearchForm");

    var data = new FormData();
    var formData = $form.serializeArray();
    $.each(formData, function (key, value) {
        data.append(this.name, this.value);
    });

    ajaxRequest('Post', $("#SearchForm").attr('action'), data, 'html', false, false).done(function (result) {
        $("#SearchTableContainer").html(result);
    });
}


function Save() {
    var $form = $("#ModelForm");
    //if ($form.valid()) {
    var data = new FormData();
    var formData = $form.serializeArray();
    $.each(formData, function (key, value) {
        data.append(this.name, this.value);
    });
    if (IsValidCustomer()) {
        
        ajaxRequest($("#ModelForm").attr('method'), $("#ModelForm").attr('action'), data, 'json', false, false).done(function (result) {

            var res = result.split(',');
            //debugger;
            if (res[0] == "success") {
                $('#myModalAddEdit').modal('hide');
                toastr.success(res[1]);
                Search();
                Clear();


            }
            else
                toastr.error(res[1]);
        });
    }
    //}
}

function Create() {
    
    var Url = "../Customers/Create";
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function Edit(id) {
    var Url = "../Customers/Edit?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function Delete(id) {
    var Url = "../Customers/Delete?Id=" + id;
    $('#myModalAddEdit').load(Url, function (response, status, xhr) {
        $('#myModalAddEdit').modal('show');
    });
}
function DeleteRow(id) {
    var Url = "../Customers/DeleteRow?Id=" + id;
    ajaxRequest("Post", Url, "", 'json', false, false).done(function (result) {
        var res = result.split(',');
        if (res[0] == "success") {
            $('#myModalAddEdit').modal('hide');
            Search();
        }
        else
            toastr.error(res[1]);
    });
}
function IsValidCustomer() {
    var isValidItem = true;
    alert("aaa");

    if ($('#PhoneNumber').val() == null)
    {
       
        isValidItem = false;
        toastr.error("من فضلك ادخل رقم الموبيل");
    }




    return isValidItem;
}
function Clear() {
    $('#Name').val() = "";

}




