﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WaterCompany.Core;
using WaterCompanyViewModel.Admin;

namespace WaterCompany.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin,Delegate")]

    public class OffersController : Controller
    {
        public List<InProductVm> _ListProduct;

        // GET: Admin/Offers
        public ActionResult Index()
        {
            //var model = "DriversSelectAll".ExecuParamsSqlOrStored(false).AsList<DriverVM>();
            return View();
        }
        public ActionResult Search()
        {
            var model = "OffersSelect".ExecuParamsSqlOrStored(false).AsList<OffersVM>();

            return PartialView(model);
        }
        [HttpGet]
        public ActionResult Create(int ProductId = 0)
        {
            // AgreementVM obj = new AgreementVM();

           
            var lst5 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst5.AddRange("ProductDrop".ExecuParamsSqlOrStored(false).AsList<EmployeeDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == ProductId ? true : false
            }).ToList());
            ViewBag.ProductId = lst5;

            TempData["AgreementProduct"] = null;
            //return PartialView("~/Areas/Agreement/Views/ManageAgreement/Create.cshtml");
            return View();
        }

        public ActionResult SearchProduct(string RequestId)
        {
            TempData.Keep();

            //var data = _InvoiceService.FindAllProducts(int.Parse(RequestId == "" ? "0" : RequestId));
            var data = "OffersDetailsSelectAll".ExecuParamsSqlOrStored(false, "InvoiceId".KVP(RequestId)).AsList<InProductVm>();
            if (data.Count() > 0)
            {
                for (int i = 0; i < data.Count(); i++)
                {
                    InProductVm obj = new InProductVm();
                    obj.Id = data[i].Id;
                    obj.Price = data[i].Price;
                    obj.Quantity = data[i].Quantity;
                    obj.TotalPrice = data[i].TotalPrice;

                    obj.NameAr = data[i].NameAr;

                    if (TempData["AgreementProduct"] != null)
                    {

                        _ListProduct = TempData["AgreementProduct"] as List<InProductVm>;
                        _ListProduct.Add(obj);
                        TempData["AgreementProduct"] = _ListProduct;

                    }
                    else
                    {
                        _ListProduct = new List<InProductVm>();
                        _ListProduct.Add(obj);
                        TempData["AgreementProduct"] = _ListProduct;
                    }

                }
                return PartialView("~/Areas/Admin/Views/Offers/SearchProduct.cshtml", _ListProduct);
            }
            else
            {
                if (TempData["AgreementProduct"] != null)
                {
                    _ListProduct = TempData["AgreementProduct"] as List<InProductVm>;
                }
                else
                {
                    _ListProduct = new List<InProductVm>();
                }

                return PartialView("~/Areas/Admin/Views/Offers/SearchProduct.cshtml", _ListProduct);
            }

        }
        [HttpPost]

        public JsonResult AddAgreementProduct(string ProductName, int ProductId, decimal Price, int Quantity)
        {
            TempData.Keep();

            InProductVm obj = new InProductVm();
            obj.Id = ProductId;
            obj.Price = Price;
            obj.Quantity = Quantity;
            obj.NameAr = ProductName;

            if (TempData["AgreementProduct"] != null)
            {

                _ListProduct = TempData["AgreementProduct"] as List<InProductVm>;
                _ListProduct.Add(obj);
                TempData["AgreementProduct"] = _ListProduct;

            }
            else
            {
                _ListProduct = new List<InProductVm>();
                _ListProduct.Add(obj);
                TempData["AgreementProduct"] = _ListProduct;
            }


            return Json("success," + "Add Success", JsonRequestBehavior.AllowGet);
        }




        [HttpPost]
        public ActionResult Create(OffersFM viewModel)
        {
            if (viewModel.Name != "")
            {
                if (viewModel.DisscountEndDate == null)
                {
                    viewModel.DisscountEndDate = Convert.ToDateTime("1/1/2000");
                }

                var result = "OffersInsert".ExecuParamsSqlOrStored(false, "Name".KVP(viewModel.Name),
               "StartDate".KVP(viewModel.StartDate), "EndDate".KVP(viewModel.EndDate), "Value".KVP(viewModel.Value),
                "Note".KVP(viewModel.Nots),
                "Disscount".KVP(viewModel.Disscount),
                "DisscountEndDate".KVP(viewModel.DisscountEndDate),

               "Id".KVP(viewModel.Id)).AsNonQuery();
                //if (data != 0)
                //{
                //    return Json("success," + "Add Success", JsonRequestBehavior.AllowGet);

                //}

                //string result = _InvoiceService.Save(viewModel);

                if (result != 0)
                {
                    var model = "OffersSelectID".ExecuParamsSqlOrStored(false).AsList<OffersLast>().LastOrDefault();
                    var resultDeleteAll = "OffersDetailsDeleteAll".ExecuParamsSqlOrStored(false, "InvoiceId".KVP(model.Id)).AsNonQuery();
                    //string resultDeleteAll = _InvoiceService.DeleteProductAll(int.Parse(result.ToString()));
                    _ListProduct = TempData["AgreementProduct"] as List<InProductVm>;
                    if (_ListProduct != null)
                    {

                        for (int i = 0; i < _ListProduct.Count(); i++)
                        {
                            result = "OffersDetailsInsertRow".ExecuParamsSqlOrStored(false, "OfferId".KVP(model.Id),
               "ProductId".KVP(_ListProduct[i].Id), "Price".KVP(_ListProduct[i].Price), "Quantity".KVP(_ListProduct[i].Quantity)
               ).AsNonQuery();
                            //result = _InvoiceService.SaveProduct(result, _ListProduct[i].Id, _ListProduct[i].Price, _ListProduct[i].Quantity);
                        }
                    }
                    return Json("success," + "Add Success", JsonRequestBehavior.AllowGet);

                    return RedirectToAction("Index","Offers");
                }
                else
                    return Json("error," + " Error", JsonRequestBehavior.AllowGet);
            }
            return View();

        }


        [HttpGet]
        public ActionResult Edit(string id,  int ProductId = 0)
        {

            var _viewModel = "OffersSelectById".ExecuParamsSqlOrStored(false, "Id".KVP(int.Parse(id))).AsList<OfferEditVM>();
            ViewBag.invoiceId = id;
           
           

          
           
            var lst5 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst5.AddRange("ProductDrop".ExecuParamsSqlOrStored(false).AsList<EmployeeDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == ProductId ? true : false
            }).ToList());
            ViewBag.ProductId = lst5;

            // return PartialView("~/Areas/Invoice/Views/ManageInvoice/Create.cshtml", _viewModel);
            return View("~/Areas/Admin/Views/Offers/Details.cshtml", _viewModel.FirstOrDefault());
        }

        public ActionResult SearchProductEdit(string RequestId)
        {
            var data = "OffersDetailsSelectAll1".ExecuParamsSqlOrStored(false, "InvoiceId".KVP(int.Parse(RequestId))).AsList<InvoiceDetailsDetails>();

            //var data = _InvoiceService.FindAllProducts(int.Parse(RequestId == "" ? "0" : RequestId));
            return PartialView("~/Areas/Admin/Views/Offers/SercheDetails.cshtml", data);

        }
    }
}