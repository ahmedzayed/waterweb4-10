﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WaterCompany.Core;
using WaterCompanyViewModel.Admin;

namespace WaterCompany.Areas.Admin.Controllers
{
    [Authorize(Roles ="Admin")]
    public class AboutUsController : Controller
    {
        public ActionResult Index()
        {
            //var model = "AboutUsSelectAll".ExecuParamsSqlOrStored(false).AsList<ManageAboutUsVM>();
            return View();
        }
        public ActionResult Search()
        {
            var model = "AboutUsSelectAll".ExecuParamsSqlOrStored(false).AsList<AboutUsVM>();

            return PartialView(model);
        }
        [HttpGet]
        public ActionResult Create()
        {
            AboutUsVM obj = new AboutUsVM();
            return PartialView("~/Areas/Admin/Views/AboutUs/Create.cshtml", obj);
        }
        [HttpPost]
        public ActionResult Create(AboutUsVM model)
        {
            var da = "AboutUsSelectAll".ExecuParamsSqlOrStored(false).AsList<AboutUsVM>().FirstOrDefault();
            if (da == null)
            {
               
                var data = "AboutUsInsert".ExecuParamsSqlOrStored(false, "Type".KVP(model.AboutDetails), "Color".KVP(model.Address),
                    "Model".KVP(model.Phonenumber),
                     "Id".KVP(model.Id)).AsNonQuery();
                if (data != 0)
                {
                    return Json("success," + "Add Success", JsonRequestBehavior.AllowGet);


                }


            }
            else
            {
                model.Id = da.Id;
                var data = "AboutUsInsert".ExecuParamsSqlOrStored(false, "Type".KVP(model.AboutDetails), "Color".KVP(model.Address),
                    "Model".KVP(model.Phonenumber),
                     "Id".KVP(model.Id)).AsNonQuery();
                if (data != 0)
                {
                    return Json("success," + "Add Success", JsonRequestBehavior.AllowGet);


                }
            }


            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var model = "AboutUsSelectById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<AboutUsVM>();
            return PartialView("~/Areas/Admin/Views/AboutUs/Create.cshtml", model.FirstOrDefault());
        }



        public ActionResult Delete(int Id)
        {
            //var data = "SP_DeleteCategory".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
          AboutUsVM obj = new AboutUsVM();
            obj.Id = Id;
            return PartialView("~/Areas/Admin/Views/AboutUs/Delete.cshtml", obj);



        }
        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "AboutUsDelete".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();

            if (data == 1)
                return Json("success," + "Delete Success", JsonRequestBehavior.AllowGet);
            else
                return Json("error," + "Error", JsonRequestBehavior.AllowGet);
        }
    }
}