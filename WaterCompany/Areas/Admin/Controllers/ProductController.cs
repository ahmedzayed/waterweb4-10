﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WaterCompany.Core;
using WaterCompanyViewModel.Admin;

namespace WaterCompany.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin,Delegate")]

    public class ProductController : Controller
    {
        // GET: Admin/Product
        public ActionResult Index()
        {
            //var model = "ProductsSelectAll".ExecuParamsSqlOrStored(false).AsList<ProductVM>();
            return View();
        }
        public ActionResult Search()
        {
            var model = "ProductsSelectAll".ExecuParamsSqlOrStored(false).AsList<ProductVM>();

            return PartialView(model);
        }
        [HttpGet]
        public ActionResult Create()
        {

            ProductVM obj = new ProductVM();
            return View( obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var model = "ProductsSelectById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<ProductVM>();
            return PartialView("~/Areas/Admin/Views/Product/Create.cshtml", model.FirstOrDefault());
        }
        [HttpPost]

        public ActionResult Create(ProductVM model)
        {
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Images/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    model.Image = "Images/" + fileNameOnly + unique + Path.GetExtension(file.FileName);

                }
            }

            
            var data = "ProductsInsert".ExecuParamsSqlOrStored(false, "NameAr".KVP(model.NameAr),
                 "DescAr".KVP(model.DescAr), "Disscount".KVP(model.Disscount),
                 "DisscountEndDate".KVP(model.DisscountEndDate),
                "Price".KVP(model.Price), "Note".KVP(model.Note), "Image".KVP(model.Image), 
                "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0 )
            {
                return RedirectToAction("Index");
            }

            else


            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int Id)
        {
            //var data = "SP_DeleteCategory".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            ProductVM obj = new  ProductVM();
            obj.Id = Id;
            return PartialView("~/Areas/Admin/Views/Product/Delete.cshtml", obj);



        }
        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "ProductsDelete".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();

            if (data == 1)
                return Json("success," + "Delete Success", JsonRequestBehavior.AllowGet);
            else
                return Json("error," + "Error", JsonRequestBehavior.AllowGet);
        }
    }
}