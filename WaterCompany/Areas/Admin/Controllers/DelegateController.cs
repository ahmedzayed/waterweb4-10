﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WaterCompany.Core;
using WaterCompany.Models;
using WaterCompanyViewModel.Admin;

namespace WaterCompany.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class DelegateController : Controller
    {
        // GET: Admin/Delegate
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;


        public ActionResult Index()
        {
            //var model = "ProductsSelectAll".ExecuParamsSqlOrStored(false).AsList<DelegateVM>();
            return View();
        }
        public ActionResult Search()
        {
            var model = "DelegatesSelect".ExecuParamsSqlOrStored(false).AsList<DelegateVM>();

            return PartialView(model);
        }
        [HttpGet]
        public ActionResult Create()
        {

            var lst = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst.AddRange("Sp_RegionDrop".ExecuParamsSqlOrStored(false).AsList<RegionVM>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.RegionList = lst;
            DelagateFM obj = new DelagateFM();
            return PartialView("~/Areas/Admin/Views/Delegate/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var lst = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst.AddRange("Sp_RegionDrop".ExecuParamsSqlOrStored(false).AsList<RegionVM>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.RegionList = lst;
            var model = "DelegatesSelectById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<DelagateFM>();
            return PartialView("~/Areas/Admin/Views/Delegate/Create.cshtml", model.FirstOrDefault());
        }
        [HttpPost]

        public async Task<ActionResult> Create(DelagateFM model)
        {




            var data = "DelegatesInsert".ExecuParamsSqlOrStored(false, "NameAr".KVP(model.NameAr),
                "NameEn".KVP(model.NameEn), "PercentAmount".KVP(model.PercentAmount),
                "Telephone".KVP(model.Telephone), "Address".KVP(model.Address),
                "Area".KVP(model.Area), "Note".KVP(model.Note),
                "RegionId".KVP(model.RegionId),
                "Id".KVP(model.Id)).AsNonQuery();

            if (data != 0)
            {
                if (model.Id == 0)
                {
                    var modeluser = "DelegatesSelect".ExecuParamsSqlOrStored(false).AsList<DelegateVM>();
                    int userid = modeluser.LastOrDefault().Id;
                    var user = new ApplicationUser { UserName = model.UserName, Email = "asa"+userid+"s@yahoo.com", UserType = true, UserId = userid };
                    var result = await UserManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {

                        UserManager.AddToRole(user.Id, "Delegate");
                        return Json("success," + "Add Success", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json("success," + "Update Success", JsonRequestBehavior.AllowGet);


                }

            }

            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int Id)
        {
            //var data = "SP_DeleteCategory".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            DelegateVM obj = new DelegateVM();
            obj.Id = Id;
            return PartialView("~/Areas/Admin/Views/Delegate/Delete.cshtml", obj);



        }
        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "DelegatesDelete".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();

            if (data == 1)
                return Json("success," + "Delete Success", JsonRequestBehavior.AllowGet);
            else
                return Json("error," + "Error", JsonRequestBehavior.AllowGet);
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
    }
}