﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WaterCompany.Core;
using WaterCompanyViewModel.Admin;

namespace WaterCompany.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class CarsController : Controller
    {
        // GET: Admin/Car
        public ActionResult Index()
        {
            //var model = "CarsSelectAll".ExecuParamsSqlOrStored(false).AsList<ManagecarsVM>();
            return View();
        }
        public ActionResult Search()
        {
            var model = "CarsSelectAll".ExecuParamsSqlOrStored(false).AsList <CarsVM>();

            return PartialView(model);
        }
        [HttpGet]
        public ActionResult Create()
        {

            CarsFM obj = new CarsFM();
            return PartialView("~/Areas/Admin/Views/Cars/Create.cshtml", obj);
        }
        [HttpPost]
        public ActionResult Create(CarsFM model)
        {

            var data = "CarsInsert".ExecuParamsSqlOrStored(false, "Type".KVP(model.TypeCars), "Color".KVP(model.ColorCars), "Model".KVP(model.ModelCars), "Number".KVP(model.NumberCars), "Not".KVP(model.NoteCars), "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "Add Success", JsonRequestBehavior.AllowGet);


            }


            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            var model = "CarsSelectById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<CarsFM>();
            return PartialView("~/Areas/Admin/Views/Cars/Create.cshtml", model.FirstOrDefault());
        }
        


        public ActionResult Delete(int Id)
        {
            //var data = "SP_DeleteCategory".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            ManagecarsVM obj = new ManagecarsVM();
            obj.Id = Id;
            return PartialView("~/Areas/Admin/Views/Cars/Delete.cshtml", obj);



        }
        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "CarsDelete".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();

            if (data == 1)
                return Json("success," + "Delete Success", JsonRequestBehavior.AllowGet);
            else
                return Json("error," + "Error", JsonRequestBehavior.AllowGet);
        }
    }
}