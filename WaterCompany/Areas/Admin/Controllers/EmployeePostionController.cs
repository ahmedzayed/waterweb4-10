﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WaterCompany.Core;
using WaterCompanyViewModel.Admin;

namespace WaterCompany.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class EmployeePostionController : Controller
    {
        // GET: Admin/EmployeePostion
        #region Actions
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Search()
        {
            var model = "EmployeePostionSelect".ExecuParamsSqlOrStored(false).AsList<EmployeePostionVM>();

            return PartialView(model);
        }
        [HttpGet]
        public ActionResult Create(int CarId = 0, int MangerId = 0)
        {


            EmployeePostionVM obj = new EmployeePostionVM();
            return PartialView("~/Areas/Admin/Views/EmployeePostion/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {

            var model = "EmployeePostionSelectById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<EmployeePostionVM>();
            return PartialView("~/Areas/Admin/Views/EmployeePostion/Create.cshtml", model.FirstOrDefault());
        }
        [HttpPost]

        public ActionResult Create(EmployeePostionVM model)
        {


            var data = "EmployeePostionInsert".ExecuParamsSqlOrStored(false, "NameAr".KVP(model.NameAr),"NameEn".KVP(model.NameEn),


                "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "Add Success", JsonRequestBehavior.AllowGet);


            }


            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int Id)
        {
            //var data = "DriversDelete".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            EmployeePostionVM obj = new EmployeePostionVM();
            obj.Id = Id;
            return PartialView("~/Areas/Admin/Views/EmployeePostion/Delete.cshtml", obj);



        }
        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "EmployeePostionDelete".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();

            if (data == 1)
                return Json("success," + "Delete Success", JsonRequestBehavior.AllowGet);
            else
                return Json("error," + "Error", JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}