﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WaterCompany.Core;
using WaterCompanyViewModel.Admin;

namespace WaterCompany.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class ContactUsController : Controller
    {
        // GET: Admin/ContactUs
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Search()
        {
            var model = "ContactSelectAll".ExecuParamsSqlOrStored(false).AsList<ContactUsVM>();

            return PartialView(model);
        }

    }
}