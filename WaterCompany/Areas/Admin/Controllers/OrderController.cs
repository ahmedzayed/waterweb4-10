﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WaterCompany.Core;
using WaterCompany.Models;
using WaterCompanyViewModel.Admin;

namespace WaterCompany.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin,Customer,Delegate")]

    public class OrderController : Controller
    {
        // GET: Admin/Order
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult Search()
        {
            if (User.IsInRole("Admin"))
            {

                var model = "GetAllOrder".ExecuParamsSqlOrStored(false).AsList<OrderVM>();

                return PartialView(model);
            }
            else if (User.IsInRole("Delegate"))
            {
                var userId = User.Identity.GetUserId();

                var UserInfo = "GetRegionByUserId".ExecuParamsSqlOrStored(false, "Id".KVP(userId)).AsList<UserInfoVM>().FirstOrDefault();
                int UserId = UserInfo.UserId;
                var UserData = "DelegatesSelectById".ExecuParamsSqlOrStored(false, "Id".KVP(UserId)).AsList<DelagateFM>().FirstOrDefault();
                int Region = UserData.RegionId;
                var model = "GetAllOrderByRegion".ExecuParamsSqlOrStored(false, "RegionId".KVP(Region)).AsList<OrderVM>();

                return PartialView(model);
            }
            else
                return PartialView();

        }
        public ActionResult Invoice()
        {

            return View();
        }
        public ActionResult AcceptInvoice(int id)
        {
            var data = "UpdateOrder".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<OrderVM>();
            //var Offers = "GetAllOrderById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<OrderFM>();
            var Order = "GetAllOrderById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<OrderFM>();

            ViewBag.Lat = Order.FirstOrDefault().Latitude;
            ViewBag.lang = Order.FirstOrDefault().Longitude;
            ////int OfferId = Offers.FirstOrDefault().OfferId;
            //var OfferDetails= "OffersDetailsSelectAll1Order".ExecuParamsSqlOrStored(false, "OfferId".KVP(OfferId)).AsList<OfferDetailsVM>();

            //foreach (var item in OfferDetails)
            //{
            //    var OrderDetailsInsert = "InsertOrderDetails".ExecuParamsSqlOrStored(false, "OrderId".KVP(id),
            //                  "ProductId".KVP(item.ProductId), "Quntity".KVP(item.Quantity), "Price".KVP(item.Price),
                             
            //                  "Id".KVP(item.Id)).AsNonQuery();

            //}


            return View();
           

        }

        public ActionResult SearchInvicoice()
        {
            if (User.IsInRole("Admin"))
            {

                var model = "GetAllOrder1".ExecuParamsSqlOrStored(false).AsList<OrderVM>();

                return PartialView(model);
            }
            else if (User.IsInRole("Delegate"))
            {
                var UserId = User.Identity.GetUserId();
                var model = "GetAllOrder1ByUserId".ExecuParamsSqlOrStored(false,"UserId".KVP(UserId)).AsList<OrderVM>();

                return PartialView(model);
            }
            else
                return PartialView();

        }
    }
}