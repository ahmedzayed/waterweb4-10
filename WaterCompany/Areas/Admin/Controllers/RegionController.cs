﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WaterCompany.Core;
using WaterCompanyViewModel.Admin;

namespace WaterCompany.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class RegionController : Controller
    {
        // GET: Admin/Region
        #region Actions
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Search()
        {
            var model = "RegionSelect".ExecuParamsSqlOrStored(false).AsList<RegionVM>();

            return PartialView(model);
        }
        [HttpGet]
        public ActionResult Create(int CarId = 0, int MangerId = 0)
        {
           

            RegionVM obj = new RegionVM();
            return PartialView("~/Areas/Admin/Views/Region/Create.cshtml", obj);
        }
        [HttpGet]
        public ActionResult Edit(int Id)
        {
          
            var model = "RegionSelectById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<RegionVM>();
            return PartialView("~/Areas/Admin/Views/Region/Create.cshtml", model.FirstOrDefault());
        }
        [HttpPost]

        public ActionResult Create(RegionVM model)
        {


            var data = "RegionInsert".ExecuParamsSqlOrStored(false, "Name".KVP(model.Name),
               
               
                "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "Add Success", JsonRequestBehavior.AllowGet);


            }


            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int Id)
        {
            //var data = "DriversDelete".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            RegionVM obj = new RegionVM();
            obj.Id = Id;
            return PartialView("~/Areas/Admin/Views/Region/Delete.cshtml", obj);



        }
        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "RegionDelete".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();

            if (data == 1)
                return Json("success," + "Delete Success", JsonRequestBehavior.AllowGet);
            else
                return Json("error," + "Error", JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}