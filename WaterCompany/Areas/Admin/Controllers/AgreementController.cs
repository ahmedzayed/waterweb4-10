﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WaterCompany.Core;
using WaterCompanyViewModel.Admin;

namespace WaterCompany.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class AgreementController : Controller
    {
        public List<AgreementProductVM> _ListProduct;

        // GET: Admin/Agreement
        #region Actions
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult Search()
        {
            var model = "AgreementsSelectAll".ExecuParamsSqlOrStored(false).AsList<AgreementVM>();

            return PartialView(model);

        }

        [HttpGet]
        public ActionResult Create(int CarId = 0, int DelegateId=0,int DriverId=0,int ProductId=0)
        {
            // AgreementVM obj = new AgreementVM();

            var lst3 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst3.AddRange("CarDrop".ExecuParamsSqlOrStored(false).AsList<CarDrop>().Select(s => new SelectListItem
            {
                Text = s.TypeCars + s.NumberCars,
                Value = s.Id.ToString(),
                Selected = s.Id == CarId ? true : false
            }).ToList());
            ViewBag.CarId = lst3;

            var lst2 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst2.AddRange("DelegateDrop".ExecuParamsSqlOrStored(false).AsList<EmployeeDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == DelegateId ? true : false
            }).ToList());
            ViewBag.DelegatId = lst2;
            var lst4 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst4.AddRange("DriverDrop".ExecuParamsSqlOrStored(false).AsList<EmployeeDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == DriverId ? true : false
            }).ToList());
            ViewBag.DriverId = lst4;
            var lst5 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst5.AddRange("ProductDrop".ExecuParamsSqlOrStored(false).AsList<EmployeeDrop>().Select(s => new SelectListItem
            {
                Text = s.NameAr,
                Value = s.Id.ToString(),
                Selected = s.Id == ProductId ? true : false
            }).ToList());
            ViewBag.ProductId = lst5;
            var lst6 = new List<SelectListItem> { new SelectListItem { Text = "الكل", Value = "0" } };
            lst6.AddRange("Sp_RegionDrop".ExecuParamsSqlOrStored(false).AsList<RegionVM>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.RegionId = lst6;

            TempData["AgreementProduct"] = null;
            //return PartialView("~/Areas/Agreement/Views/ManageAgreement/Create.cshtml");
            return View();
        }

        public decimal getProductPrice(string productId)
        {
            //var res = _commonService.FindProductPrice(int.Parse(productId));
            var model = "ProductPriceSelect".ExecuParamsSqlOrStored(false, "productId".KVP(productId)).AsList<PriceSelect>().FirstOrDefault();
            return (model.Price);

        }

        [HttpPost]

        public JsonResult AddAgreementProduct(string ProductName, int ProductId, decimal Price, string Target)
        {
            TempData.Keep();

            AgreementProductVM obj = new AgreementProductVM();
            obj.Id = ProductId;
            obj.Price = Price;
            obj.Target = Target;
            obj.Name = ProductName;

            if (TempData["AgreementProduct"] != null)
            {

                _ListProduct = TempData["AgreementProduct"] as List<AgreementProductVM>;
                _ListProduct.Add(obj);
                TempData["AgreementProduct"] = _ListProduct;

            }
            else
            {
                _ListProduct = new List<AgreementProductVM>();
                _ListProduct.Add(obj);
                TempData["AgreementProduct"] = _ListProduct;
            }


            return Json("success," + "Add Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchProduct(string RequestId)
        {
            TempData.Keep();

            var data = "AgreementsProductsSelectAll".ExecuParamsSqlOrStored(false, "RequestId".KVP(RequestId)).AsList<AgreementProductVM>();

            //var data = _AgreementService.FindAllProducts(int.Parse(RequestId == "" ? "0" : RequestId));
            if (data.Count() > 0)
            {
                for (int i = 0; i < data.Count(); i++)
                {
                    AgreementProductVM obj = new AgreementProductVM();
                    obj.Id = data[i].Id;
                    obj.Price = data[i].Price;
                    obj.Target = data[i].Target;
                    obj.Name = data[i].Name;

                    if (TempData["AgreementProduct"] != null)
                    {

                        _ListProduct = TempData["AgreementProduct"] as List<AgreementProductVM>;
                        _ListProduct.Add(obj);
                        TempData["AgreementProduct"] = _ListProduct;

                    }
                    else
                    {
                        _ListProduct = new List<AgreementProductVM>();
                        _ListProduct.Add(obj);
                        TempData["AgreementProduct"] = _ListProduct;
                    }

                }
                return PartialView("~/Areas/Agreement/Views/Agreement/SearchProduct.cshtml", _ListProduct);
            }
            else
            {
                if (TempData["AgreementProduct"] != null)
                {
                    _ListProduct = TempData["AgreementProduct"] as List<AgreementProductVM>;
                }
                else
                {
                    _ListProduct = new List<AgreementProductVM>();
                }

                return PartialView("~/Areas/Admin/Views/Agreement/SearchProduct.cshtml", _ListProduct);
            }

        }
        [HttpPost]
        public JsonResult Create(AgreementFM viewModel)
        {

            var result = "AgreementsInsert".ExecuParamsSqlOrStored(false, "StartDate".KVP(viewModel.StartDate),
               "EndDate".KVP(viewModel.EndDate), "Area".KVP(viewModel.Area), "DelegatId".KVP(viewModel.DelegatId),
               "CarId".KVP(viewModel.CarId), "DriverId".KVP(viewModel.DriverId), "Note".KVP(viewModel.Note),
               "RegionId".KVP(viewModel.RegionId),

               "Id".KVP(viewModel.Id)).AsNonQuery();

            //string result = _AgreementService.Save(viewModel);

            if (result != 0)
            {
                var model = "AgreementsSelectAll".ExecuParamsSqlOrStored(false).AsList<AgreementVM>().LastOrDefault();
                var resultDeleteAll = "AgreementsProductsDeleteAll".ExecuParamsSqlOrStored(false, "RequestId".KVP(model.Id)).AsNonQuery();
                //string resultDeleteAll = _AgreementService.DeleteProductAll(int.Parse(result));
                _ListProduct = TempData["AgreementProduct"] as List<AgreementProductVM>;
                if (_ListProduct != null)
                {

                    for (int i = 0; i < _ListProduct.Count(); i++)
                    {
                        result = "AgreementsProductsInsert".ExecuParamsSqlOrStored(false, "RequestId".KVP(model.Id),
             "ProductId".KVP(_ListProduct[i].Id), "Price".KVP(_ListProduct[i].Price), "TargetSelling".KVP(_ListProduct[i].Target)
             ).AsNonQuery();

                        //result = _AgreementService.SaveProduct(int.Parse(result), _ListProduct[i].Id, _ListProduct[i].Price, _ListProduct[i].Target, viewModel.CreatedBy);
                    }
                }
                return Json("success," + "Add Success", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + " Error", JsonRequestBehavior.AllowGet);

        }
        //[HttpPost]
        //public JsonResult Create(AgreementVM viewModel)
        //{

        //    viewModel.CreatedBy = 1;

        //    string result = _AgreementService.Save(viewModel);
        //    if (result != "0")
        //    {
        //        string resultDeleteAll = _AgreementService.DeleteProductAll(int.Parse(result));
        //        _ListProduct = TempData["AgreementProduct"] as List<ProductVm>;
        //        if (_ListProduct != null)
        //        {

        //            for (int i = 0; i < _ListProduct.Count(); i++)
        //            {
        //                result = _AgreementService.SaveProduct(int.Parse(result), _ListProduct[i].Id, _ListProduct[i].Price, _ListProduct[i].Target, viewModel.CreatedBy);
        //            }
        //        }
        //        return Json("success," + "Add Success", JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //        return Json("error," + " Error", JsonRequestBehavior.AllowGet);

        //}

        //[HttpGet]
        //public ActionResult Edit(string id)
        //{

        //    AgreementVM _viewModel = _AgreementService.GetById(int.Parse(id));

        //    ViewBag.CarId = _commonService.FindCars(_viewModel.CarId);
        //    ViewBag.DelegatId = _commonService.FindDelegates(_viewModel.DelegatId);
        //    ViewBag.DriverId = _commonService.FindDrivers(_viewModel.DriverId);
        //    ViewBag.ProductId = _commonService.FindProducts();

        //    // return PartialView("~/Areas/Agreement/Views/ManageAgreement/Create.cshtml", _viewModel);
        //    return View("~/Areas/Agreement/Views/ManageAgreement/Edit.cshtml", _viewModel);
        //}

        //[HttpPost]
        //public JsonResult Edit(AgreementVM viewModel)
        //{
        //    if (viewModel.DelegatId != 0)
        //    {
        //        viewModel.CreatedBy = 1;

        //        string result = _AgreementService.Save(viewModel);
        //        if (result != "0")
        //        {
        //            return Json("success," + "Add Success", JsonRequestBehavior.AllowGet);
        //        }
        //        else
        //            return Json("error," + " Error", JsonRequestBehavior.AllowGet);
        //    }
        //    return null;

        //}

        //[HttpGet]
        //public ActionResult Delete(int id)
        //{
        //    AgreementVM obj = new AgreementVM();
        //    obj.Id = id;
        //    return PartialView("~/Areas/Agreement/Views/ManageAgreement/Delete.cshtml", obj);
        //}


        //[HttpPost]

        //public JsonResult DeleteRow(int Id)
        //{
        //    string result = _AgreementService.Delete(Id);

        //    if (result == "1")
        //        return Json("success," + "Delete Success", JsonRequestBehavior.AllowGet);
        //    else
        //        return Json("error," + "Error", JsonRequestBehavior.AllowGet);
        //}

        //public JsonResult getProductPrice(string productId)
        //{
        //    var res = _commonService.FindProductPrice(int.Parse(productId));
        //    return Json(res);
        //}
        #endregion
    }
}