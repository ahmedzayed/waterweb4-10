﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WaterCompany.Core;
using WaterCompanyViewModel.Admin;

namespace WaterCompany.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]

    public class CondtionsController : Controller
    {

            // GET: Admin/Condtion
            #region Actions
            public ActionResult Index()
            {
                return View();
            }
            public ActionResult Search()
            {
                var model = "CondtionSelect".ExecuParamsSqlOrStored(false).AsList<CondtionVM>();

                return PartialView(model);
            }
            [HttpGet]
            public ActionResult Create(int CarId = 0, int MangerId = 0)
            {


                CondtionVM obj = new CondtionVM();
                return PartialView("~/Areas/Admin/Views/Condtions/Create.cshtml", obj);
            }
            [HttpGet]
            public ActionResult Edit(int Id)
            {

                var model = "CondtionSelectById".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsList<CondtionVM>();
                return PartialView("~/Areas/Admin/Views/Condtions/Create.cshtml", model.FirstOrDefault());
            }
            [HttpPost]

            public ActionResult Create(CondtionVM model)
            {

                if (model.Details == null)
                {
                    return Json("error," + " من فضلك ادخل الشرط", JsonRequestBehavior.AllowGet);

                }

                var data = "CondtionInsert".ExecuParamsSqlOrStored(false, "Details".KVP(model.Details),


                    "Id".KVP(model.Id)).AsNonQuery();
                if (data != 0)
                {
                    return Json("success," + "Add Success", JsonRequestBehavior.AllowGet);


                }


                return Json("error," + " Error", JsonRequestBehavior.AllowGet);
            }

            public ActionResult Delete(int Id)
            {
                //var data = "DriversDelete".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
                CondtionVM obj = new CondtionVM();
                obj.Id = Id;
                return PartialView("~/Areas/Admin/Views/Condtions/Delete.cshtml", obj);



            }
            [HttpPost]

            public JsonResult DeleteRow(int Id)
            {
                var data = "CondtionDelete".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();

                if (data == 1)
                    return Json("success," + "Delete Success", JsonRequestBehavior.AllowGet);
                else
                    return Json("error," + "Error", JsonRequestBehavior.AllowGet);
            }
            #endregion 
        }
    }


    